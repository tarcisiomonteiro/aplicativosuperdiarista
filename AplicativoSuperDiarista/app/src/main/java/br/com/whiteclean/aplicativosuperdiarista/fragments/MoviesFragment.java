package br.com.whiteclean.aplicativosuperdiarista.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.whiteclean.aplicativosuperdiarista.R;

/**
 * Created by Rafael on 24/11/2016.
 */
public class MoviesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_movies, container, false);

        return rootView;
    }
}
