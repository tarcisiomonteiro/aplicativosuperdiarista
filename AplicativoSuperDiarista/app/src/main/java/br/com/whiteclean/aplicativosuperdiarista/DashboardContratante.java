package br.com.whiteclean.aplicativosuperdiarista;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import br.com.whiteclean.aplicativosuperdiarista.Services.GetUsuario;
import br.com.whiteclean.aplicativosuperdiarista.fragments.FragmentFacebookLogin;
import br.com.whiteclean.aplicativosuperdiarista.model.Device;
import br.com.whiteclean.aplicativosuperdiarista.model.Usuario;

public class DashboardContratante extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final int LOGIN = 0;
    private static final String STATE_SELECTED_FRAGMENT_INDEX = "selected_fragment_index";
    public static final String FRAGMENT_TAG = "fragment_tag";
    private FragmentManager mFragmentManager;
    private boolean isLogado = false;
    public String deviceId;
    Device device = new Device();
    Intent intent = new Intent();
    Bundle params = new Bundle();
    Usuario usuario = new Usuario();
    String perfil;
    private TextView Textv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_contratante);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        new GetUsuario().execute("https://diaristas.herokuapp.com/api/usuario");
        mFragmentManager = getSupportFragmentManager();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard_contratante, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.pagina_inicial) {
            return true;
        } else if(id == R.id.simple_login){
            toggleFragment(LOGIN);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = new Intent();

        int id = item.getItemId();

        if (id == R.id.buscarDiaristas) {
            intent = new Intent(getApplicationContext(), ListaDiaristas.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Opção Buscar Diaristas", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.agenda) {
            intent = new Intent(getApplicationContext(), AgendaActivity.class);
            startActivity(intent);
        } else if (id == R.id.historico) {
            intent = new Intent(getApplicationContext(), HistoricoActivity.class);
            startActivity(intent);
        } else if (id == R.id.pontuacao) {
            intent = new Intent(getApplicationContext(), AvaliacaoActivity.class);
            startActivity(intent);
        } else if (id == R.id.solicitacoes) {
            intent = new Intent(getApplicationContext(), AvaliacaoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            intent = new Intent(getApplicationContext(), AvaliacaoActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void toggleFragment(int index) {
        Fragment fragment = mFragmentManager.findFragmentByTag(FRAGMENT_TAG);
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        switch (index){
            case LOGIN:
                transaction.replace(android.R.id.content, new FragmentFacebookLogin(), FRAGMENT_TAG);
                break;
        }
        transaction.commit();
    }
}
