package br.com.whiteclean.aplicativosuperdiarista;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;

public class SMS extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public void onClick(View v) {
            String phoneNumber = ((EditText) findViewById(R.id.editView1)).getText().toString();
            try {
                SmsManager.getDefault().sendTextMessage(phoneNumber, null, "Olá, gostaria de saber sua disponibilidade.", null, null);
            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                AlertDialog dialog = alertDialogBuilder.create();

                dialog.setMessage(e.getMessage());
                dialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 1000);

            }
    }
}

