package  br.com.whiteclean.aplicativosuperdiarista.model;

import java.util.Date;

public class Servico {
	
	private int idServico;
	private Boolean aceito;
	private Usuario usuarioContrata;
	private Diarista usuarioDiari;
	private int tipoServico;
	private int avaliacao;
    private Date dataSolicitacao;
	
	public Servico (){}

    public Servico(int idServico, Boolean aceito, Usuario usuarioContrata, Diarista usuarioDiari, int tipoServico, int avaliacao, Date dataSolicitacao) {
        this.idServico = idServico;
        this.aceito = aceito;
        this.usuarioContrata = usuarioContrata;
        this.usuarioDiari = usuarioDiari;
        this.tipoServico = tipoServico;
        this.avaliacao = avaliacao;
        this.dataSolicitacao = dataSolicitacao;
    }

    public Boolean getAceito() {
        return aceito;
    }

    public void setAceito(Boolean aceito) {
        this.aceito = aceito;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public int getAvaliacao() {
		return avaliacao;
	}

	public void setAvaliacao(int avaliacao) {
		this.avaliacao = avaliacao;
	}

	public Usuario getUsuarioContrata() {
		return usuarioContrata;
	}

	public void setUsuarioContrata(Usuario usuarioContrata) {
		this.usuarioContrata = usuarioContrata;
	}

	public Diarista usuarioDiari() {
		return usuarioDiari;
	}

	public void setusuarioDiari(Diarista getUsuarioDiari) {	this.usuarioDiari = usuarioDiari; }

	public int getIdServico() {
		return idServico;
	}

	public void setIdServico(int idServico) {
		this.idServico = idServico;
	}

	public int getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}

	@Override
	public String toString() {
		return "Servico{" +
				"idServico=" + idServico +
				", usuarioContrata='" + usuarioContrata + '\'' +
				", getUsuarioDiari='" + usuarioDiari + '\'' +
				", tipoServico=" + tipoServico +
				", avaliacao=" + avaliacao +
				'}';
	}
}
