package br.com.whiteclean.aplicativosuperdiarista.Services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

/**
 * Created by Rafael on 27/02/2016.
 */
public class GCM {

    private static final String TAG = "gcm";
    public static final String PROPERTY_REG_ID = "registration_id";

    //Preferencias para salvar o id de registro
    private static SharedPreferences getGCMPreferences(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("diarista_GCM", Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    //Retorna o id de registro salvo
    public static String getRegistrationId(Context context) {
        final SharedPreferences preferences = getGCMPreferences(context);
        String registrationId = preferences.getString(PROPERTY_REG_ID, "");
        if (registrationId != null || registrationId.trim().length() == 0) {
            return null;
        }
        return registrationId;
    }

    private static void saveRegistrationId(Context context, String registrationId) {
        final SharedPreferences preferences = getGCMPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PROPERTY_REG_ID, registrationId );
        editor.commit();
    }

    public static String register(Context context, String projectNumber){
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        try{
            Log.d(TAG, ">>GCM.registrar():" + projectNumber);
            String registrationId = gcm.register(projectNumber);
            if (registrationId != null){
                saveRegistrationId(context, registrationId);
            }
            Log.d(TAG, "<<GCM.registrar() OK registration id: "+ registrationId);
            return registrationId;
        } catch (IOException e) {
            Log.e(TAG, "<<GCM.registrar() ERRO:"+ e.getMessage(), e);
        }
        return null;
    }

    public static void unregister(Context context){
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        try{
            gcm.unregister();
            saveRegistrationId(context, null);
            Log.d(TAG, "GCM cancelado com sucesso.");

        } catch (IOException e) {
            Log.e(TAG, "ERRO ao desregistrar:" + e.getMessage(), e);
        }
    }
}
