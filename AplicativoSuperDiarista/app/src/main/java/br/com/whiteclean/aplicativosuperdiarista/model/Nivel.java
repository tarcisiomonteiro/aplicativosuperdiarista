package br.com.whiteclean.aplicativosuperdiarista.model;

/**
 * Created by Rafael on 24/01/2016.
 */
public class Nivel {


    private int nivel_id;
    private NIVEL_DESCRI nome;

    public Nivel() {}

    public Nivel(int nivel_id, NIVEL_DESCRI nome) {
        super();
        this.nivel_id = nivel_id;
        this.nome = nome;
    }

    public int getNivel_id() {
        return nivel_id;
    }
    public void setNivel_id(int nivel_id) {
        this.nivel_id = nivel_id;
    }

    public NIVEL_DESCRI getNome() {
        return nome;
    }
    public void setNome(NIVEL_DESCRI nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Nivel [nivel_id=" + nivel_id + ", nome=" + nome + "]";
    }
}
