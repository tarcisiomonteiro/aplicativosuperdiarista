package br.com.whiteclean.aplicativosuperdiarista;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.timessquare.CalendarPickerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;


public class BuscaDiaristaActivity extends AppCompatActivity {

    public Intent intent;
    private Button button;
    public boolean logged;
    private CalendarPickerView calendar;
    Diarista diarista = new Diarista();
    Date today = new Date();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busca);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FacebookSdk.sdkInitialize(getApplicationContext());

        button = (Button) findViewById(R.id.button2);
        button = (RadioButton) findViewById(R.id.porBairro);
        button = (RadioButton) findViewById(R.id.porCidade);

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        calendar = (CalendarPickerView) findViewById(R.id.calendar_view2);
        calendar.init(today, nextYear.getTime()).withSelectedDate(today).inMode(CalendarPickerView.SelectionMode.MULTIPLE);
    }

    public void onRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.porBairro:
	            if (checked){
                    Toast.makeText(getApplicationContext(), "Buscando diaristas...", Toast.LENGTH_LONG).show();
	            	startActivity(new Intent(this, ListaDiaristas.class));
	            }
	            break;
	        case R.id.porCidade:
	            if (checked)
                    Toast.makeText(getApplicationContext(), "Buscando diaristas...", Toast.LENGTH_LONG).show();
	                // tras a lista de cidades para opção
	            break;
	    }
	}

    public List<Date> selecionaDataServico(View view) {
        List<Date> disponibilidade;
        disponibilidade = calendar.getSelectedDates();
        diarista.setDisponiblidade(disponibilidade);
        return disponibilidade;

    }
    public void mostraDiaristas(View view){
        selecionaDataServico(view);
       Intent intent = new Intent(getApplicationContext(), ListaDiaristas.class);
       startActivity(intent);
    }

    public void iniciaLogin(View view){
        intent = new Intent(getApplicationContext(), DashboardDiarista.class);
        startActivity(intent);

        String url = "https://diaristas.herokuapp.com/api/usuario";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response).getJSONObject("form");
                    String site = jsonResponse.getString("site"), network = jsonResponse.getString("network");
                    System.out.println("Site: " + site + "\nNetwork: " + network);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("nome", "Limpador");
                params.put("email", "Lavar roupa");
                params.put("telefone", "94939586");
                params.put("cidade", "Porto Alegre");
                params.put("nivel", "2");

                return params;
            }
        };
        Volley.newRequestQueue(this).add(postRequest);
    }
}
