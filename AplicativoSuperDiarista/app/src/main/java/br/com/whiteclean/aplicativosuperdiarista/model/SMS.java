package br.com.whiteclean.aplicativosuperdiarista.model;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

/**
 * Created by Rafael on 21/04/2016.
 */
public class SMS {

    private static final String TAG = "DIARISTAS";

    public void send(Context context, String to, String msg){
        try{
            SmsManager smsManager = SmsManager.getDefault();
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(), 0);
            smsManager.sendTextMessage(to, null, msg, pendingIntent, null);
            Log.d(TAG, "Sms.send to["+to+"] msg["+msg+"]");
        }catch (Exception e){
            Log.e(TAG, "ERRO ao enviar o SMS:" + e.getMessage(), e);
        }
    }
}
