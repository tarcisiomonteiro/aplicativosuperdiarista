package br.com.whiteclean.aplicativosuperdiarista;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

public class AvaliacaoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliacao);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
    @Override
    public void onBackPressed() {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }

    public void onClickAvalia(View v){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
       // this.finish();
        Toast.makeText(getApplicationContext(), "Obrigado por avaliar esse serviço.", Toast.LENGTH_LONG).show();
    }

    public void iniciaRecontrata(View v){
        Intent intent = new Intent(getApplicationContext(), BuscaDiaristaActivity.class);
        startActivity(intent);
    }
}
