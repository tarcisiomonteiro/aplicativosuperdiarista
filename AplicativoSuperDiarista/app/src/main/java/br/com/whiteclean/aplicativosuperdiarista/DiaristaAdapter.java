package br.com.whiteclean.aplicativosuperdiarista;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;

public class DiaristaAdapter extends RecyclerView.Adapter<DiaristaAdapter.MyViewHolder> {
    private List<Diarista> listaDiarista;

    public DiaristaAdapter(){

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nome, email, fone, nivel;
        public RatingBar avaliacao;
        public ArrayList<Date> disponibilidade;

        public MyViewHolder(View view) {
            super(view);
            nome = (TextView) view.findViewById(R.id.nome);
            email = (TextView) view.findViewById(R.id.email);
            fone = (TextView) view.findViewById(R.id.fone);
            nivel = (TextView) view.findViewById(R.id.nivel_diarista);
            avaliacao = (RatingBar) view.findViewById(R.id.avaliacao);
        }
    }

    public DiaristaAdapter(List<Diarista> listaDiarista) {
        this.listaDiarista = listaDiarista;
    }

    @Override
    public DiaristaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_diarista, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DiaristaAdapter.MyViewHolder holder, int position) {

            Diarista diarista = listaDiarista.get(position);
            holder.nome.setText("Nome: " + diarista.getNome());
            holder.email.setText("Email: " + diarista.getEmail());
            holder.fone.setText("Telefone: " + diarista.getTelefone());
            holder.nivel.setText("Nivel: " + diarista.getNivel());
            holder.avaliacao.setNumStars((int) diarista.getAvaliacao());
        }

    @Override
    public int getItemCount() {
        return listaDiarista.size();
    }
}
