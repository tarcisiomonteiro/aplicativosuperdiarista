package br.com.whiteclean.aplicativosuperdiarista.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.DividerItemDecoration;
import br.com.whiteclean.aplicativosuperdiarista.R;
import br.com.whiteclean.aplicativosuperdiarista.RequestAdapter;
import br.com.whiteclean.aplicativosuperdiarista.RequestAdapterRealizado;
import br.com.whiteclean.aplicativosuperdiarista.model.Request;

/**
 * Created by Rafael on 24/11/2016.
 */
public class ServicosRealizadosFragment extends Fragment {

    private Request request;
    private List<Request> listaRequests = new ArrayList<>();
    private RecyclerView recyclerView;
    private RequestAdapterRealizado mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_top_rated, container, false);
            recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_realizado);
            mAdapter = new RequestAdapterRealizado(listaRequests);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);

            prepareRequests();
            return rootView;

        }

    //método que busca os requests realizados para esse usuário
    private void prepareRequests() {
        String url = "https://diaristas.herokuapp.com/api/request";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                try {
                    if (response.length() > 0) {
                        listaRequests.clear();
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            Request request = new Request();
                            request.usuarioRequerente = jsonObject.getString("diarista");
                            request.horario = jsonObject.getString("horario");
                            request.endereco = jsonObject.getString("endereco");
                            request.data = jsonObject.getString("dataPedido");

                            listaRequests.add(i, request);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
        };
        Volley.newRequestQueue(getContext()).add(jsonArrayRequest);
    }
}
