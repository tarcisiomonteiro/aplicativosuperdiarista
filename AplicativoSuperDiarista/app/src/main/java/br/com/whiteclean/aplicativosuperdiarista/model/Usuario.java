package  br.com.whiteclean.aplicativosuperdiarista.model;

public class Usuario {

private String usuario_id;
private String username;
private String tipoUsuario;
private String nome;
private String endereco;
private String telefone;

public Usuario(String id, String username, String nome, String endereco, String telefone, String tipoUsuario) {
	this.usuario_id = id;
	this.username = username;
	this.tipoUsuario = tipoUsuario;
	this.nome = nome;
	this.endereco = endereco;
	this.telefone = telefone;
}

public Usuario() {

}

public String getId() {
	return usuario_id;
}

public void setId(String id) {
	this.usuario_id = id;
}

public String getNome() {
	return nome;
}

public void setNome(String nome) {
	this.nome = nome;
}

public String getUsername() {
		return username;
	}

public void setUsername(String username) {
		this.username = username;
	}

public String getEndereco() {
	return endereco;
}

public void setEndereco(String endereco) {
	this.endereco = endereco;
}

public String getTelefone() {
	return telefone;
}

public void setTelefone(String telefone) {
	this.telefone = telefone;
}

public String getTipoUsuario() {
	return tipoUsuario;
}

public void setTipoUsuario(String tipoUsuario) {
	this.tipoUsuario = tipoUsuario;
}

	@Override
	public String toString() {
		return "Usuario{" +
				"usuario_id='" + usuario_id + '\'' +
				", username='" + username + '\'' +
				", tipoUsuario='" + tipoUsuario + '\'' +
				", nome='" + nome + '\'' +
				", endereco='" + endereco + '\'' +
				", telefone='" + telefone + '\'' +
				'}';
	}
}
