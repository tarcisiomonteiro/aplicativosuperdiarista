package br.com.whiteclean.aplicativosuperdiarista;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.timessquare.CalendarPickerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;
import br.com.whiteclean.aplicativosuperdiarista.model.NIVEL_DESCRI;

public class RegistraHorarioActivity extends AppCompatActivity {

    Date today = new Date();
    public CalendarPickerView calendar;
    public int pontuacao = 0;
    public int pontosRecebidos;
    Diarista diarista = new Diarista();
    public boolean logged = true;
    Intent intent = new Intent();
    public TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registra);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        calendar = (CalendarPickerView) findViewById(R.id.calendar_view);
        calendar.init(today, nextYear.getTime()).withSelectedDate(today).inMode(CalendarPickerView.SelectionMode.MULTIPLE);
       //calendar.highlightDates(getAnualHolidays());

        getUsuario(diarista);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String opcao = "Opção: " + item.toString();

        switch (id) {
            case R.id.sobre:
                Toast.makeText(this, opcao, Toast.LENGTH_LONG).show();
                return true;

            case R.id.home:
                Toast.makeText(this, opcao, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), DashboardDiarista.class);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                return true;

           /* case R.id.action_cadastra:
                //ArrayList<Date> selectedDates = (ArrayList<Date>) calendar.getSelectedDates();
                //Toast.makeText(getApplicationContext(), selectedDates.toString(), Toast.LENGTH_SHORT).show();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

   /* @Override
    protected Dialog onCreateDialog(int id) {
        if (R.id.calendar_view == id) {
            return new DatePickerDialog(this, listener, ano, mes, dia);
        }
        return null;
    }*/

    public void getUsuario(Diarista diarista){
        diarista.getNome();
        diarista.getTelefone();
        diarista.getDisponiblidade();
    }

    public int adicionaPontos() {
        pontuacao = pontuacao + pontosRecebidos;
        return pontuacao;
    }

    public boolean primeiroCadastro() {
        boolean primeiro = false;
        if (diarista.disponiblidade == null) {
            primeiro = true;
        }
        return primeiro;
    }

    public void mostrarEvolucao(View view) {
        pontosRecebidos = 25;
        adicionaPontos();
        diarista.pontuacao = pontuacao;
        diarista.setNivel();

        diarista.setDisponiblidade(selecionaDisponibilidadeClick(view));
        registraDiarista(view);

        if (logged == true) {
            if (primeiroCadastro()) {
                adicionaPontos();
                diarista.setNivel();
                diarista.setPontuacao(pontuacao);
                registraDiarista(view);
                selecionaDisponibilidadeClick(view);
                Toast.makeText(getApplicationContext(),
                        "Parabéns você realizou uma das ações especiais e ganhou" + pontosRecebidos + "Extras, sua pontuação atual é: "
                                + diarista.pontuacao + "\n" + diarista.nivel +
                                diarista.disponiblidade + " pontos", Toast.LENGTH_SHORT).show();
            }
            intent = new Intent(getApplicationContext(), RegistraTipoServico.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Parabéns" +diarista.nome+ " você ganhou" +
                            pontosRecebidos + " pontos!" + ", sua pontuação atual é: "
                            + diarista.pontuacao + "\n" + diarista.nivel + " pontos",
                    Toast.LENGTH_LONG).show();

        } else {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } this.finish();
    }

    public List<Date> selecionaDisponibilidadeClick(View view) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        List<Date> disponibilidade;
            disponibilidade = calendar.getSelectedDates();
            diarista.setDisponiblidade(disponibilidade);
        try {
            sdf.parse(String.valueOf(disponibilidade));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return disponibilidade;

    }

    public void registraDiarista(View view) {

        String url = "https://diaristas.herokuapp.com/api/usuario";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response).getJSONObject("form");
                    String site = jsonResponse.getString("site"), network = jsonResponse.getString("network");

                    //classe EscolhaActivity deve passar os dados do perfil Diarista por parametros Extra
                    diarista.setNome(diarista.getNome());
                    diarista.setCidade(diarista.getCidade());
                    diarista.setEmail(diarista.getEmail());
                    diarista.setNivel(diarista.getNivel());
                    diarista.setCidade(diarista.getCidade());
                    diarista.setTelefone(diarista.getTelefone());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("email", diarista.getEmail());
                params.put("cidade", diarista.getCidade());
                params.put("nome", diarista.getNome());
                params.put("nivel", String.valueOf(diarista.getNivel()));
                params.put("telefone", diarista.getTelefone());
                params.put("avaliacao", String.valueOf(diarista.getAvaliacao()));
                params.put("disponibilidade", String.valueOf(diarista.getDisponiblidade()));
                return params;
            }
        };
        Volley.newRequestQueue(this).add(postRequest);
    }

    private ArrayList<Date> getAnualHolidays() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        String diaDoTrabalho = "01/05/2017";
        String corpusChristi = "26/05/2017";
        String independencia = "07/09/2017";
        String farroupilha = "20/09/2017";
        String senhoraAparecida = "07/10/2017";
       // String finados = "02/11/2016";
        //String procRepublica = "15/11/2016";
        String natal = "25/12/2016";
        String anoNovo = "01/01/2017";

        Date natalParse = null;
        Date anoNovoParse = null;
        Date diaDoTrabalhoParse = null;
        Date corpusChristiParse = null;
        Date independenciaParse = null;
        Date senhoraAparecidaParse = null;
       //Date finadosParse = null;
        Date proclamacao = null;
        Date farroupilhaParse = null;

        try {

            natalParse = sdf.parse(natal);
            anoNovoParse = sdf.parse(anoNovo);
            diaDoTrabalhoParse = sdf.parse(diaDoTrabalho);
            corpusChristiParse = sdf.parse(corpusChristi);
            farroupilhaParse = sdf.parse(farroupilha);
            independenciaParse = sdf.parse(independencia);
            senhoraAparecidaParse = sdf.parse(senhoraAparecida);
            //finadosParse = sdf.parse(finados);
            //proclamacao = sdf.parse(procRepublica);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        ArrayList<Date> holidays = new ArrayList<>();

        holidays.add(natalParse);
        holidays.add(anoNovoParse);
        //holidays.add(proclamacao);
        holidays.add(diaDoTrabalhoParse);
        holidays.add(corpusChristiParse);
        holidays.add(farroupilhaParse);
        holidays.add(independenciaParse);
        holidays.add(senhoraAparecidaParse);
        //holidays.add(finadosParse);

        return holidays;
    }
}
