package br.com.whiteclean.aplicativosuperdiarista.Services;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Map;

import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;

/**
 * Created by I850210 on 30/11/2015.
 */
public class PUTService extends AsyncTask<String, Integer, JSONObject > {

    private int PERIOD = 1500;
    private boolean run = true;
    URL url = null;

    public void kill() {
        this.run = false;
    }

   /* @Override
    protected Void doInBackground(Object... params) {

*/
    @Override
    protected JSONObject doInBackground(String... params) {

        JSONObject jsonParam = null;
            try {
                Thread.sleep(PERIOD);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }  try {
                url = new URL("https://diaristas.herokuapp.com/api/device");
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                httpCon.setDoOutput(true);
                // here you are setting the `Content-Type` for the data you are sending which is `application/json`
                httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                httpCon.connect();

               OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
                out.write("Resource content");
                out.close();

                InputStream inputStream = httpCon.getInputStream();
                int input = httpCon.getResponseCode();
                Log.d("Diarista", "OK");

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("Hummm", "not good");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

}