package br.com.whiteclean.aplicativosuperdiarista;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by Rafael on 20/03/2016.
 */

public class SplashScreenContratante extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_contratante);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                    startActivity(new Intent(getBaseContext(), BuscaDiaristaActivity.class));
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    finish();
                }
        }, 1500);
    }

//    public class VerificaLogado extends AsyncTask<String , Integer, Boolean> {
//        @Override
//        protected Boolean doInBackground(String... params) {
//            Boolean logado = false;
//
//             if (logado == true){
//
//            }
//            return null;
//        }
//    }
}
