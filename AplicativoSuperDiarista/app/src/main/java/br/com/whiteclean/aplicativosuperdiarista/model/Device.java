package br.com.whiteclean.aplicativosuperdiarista.model;

/**
 * Created by Rafael on 09/04/2016.
 */
public class Device {

    public String Id;
    public Boolean perfil;

    public Device(String id, Boolean perfil) {
        Id = id;
        this.perfil = perfil;
    }

    public Device() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public Boolean getPerfil() {
        return perfil;
    }

    public void setPerfil(Boolean perfil) {
        this.perfil = perfil;
    }

    @Override
    public String toString() {
        return "Device{" +
                "Id=" + Id +
                ", perfil='" + perfil + '\'' +
                '}';
    }
}
