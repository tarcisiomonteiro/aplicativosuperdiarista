package  br.com.whiteclean.aplicativosuperdiarista.model;

public class Bairro {

	private int idBairro;
	private String nomeBairro;
	private Cidade cidade; 
	
	public Bairro(int idBairro, String nomeBairro, Cidade cidade) {
		this.idBairro = idBairro;
		this.nomeBairro = nomeBairro;
		this.cidade = cidade;
	}
	
	public Bairro(int idBairro, String nomeBairro) {
		this.idBairro = idBairro;
		this.nomeBairro = nomeBairro;
	}

	public int getIdBairro() {
		return idBairro;
	}

	public void setIdBairro(int idBairro) {
		this.idBairro = idBairro;
	}

	public String getNomeBairro() {
		return nomeBairro;
	}

	public void setNomeBairro(String nomeBairro) {
		this.nomeBairro = nomeBairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Bairro [idBairro=" + idBairro + ", nomeBairro=" + nomeBairro
				+ ", cidade=" + cidade + "]";
	}
	
	
	
}
