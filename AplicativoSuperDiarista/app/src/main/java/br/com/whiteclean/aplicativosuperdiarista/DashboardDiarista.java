package br.com.whiteclean.aplicativosuperdiarista;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.fragments.FragmentFacebookLogin;
import br.com.whiteclean.aplicativosuperdiarista.model.Request;
import br.com.whiteclean.aplicativosuperdiarista.model.Usuario;

public class DashboardDiarista extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final int LOGIN = 0;
    private static final String STATE_SELECTED_FRAGMENT_INDEX = "selected_fragment_index";
    public static final String FRAGMENT_TAG = "fragment_tag";
    private FragmentManager mFragmentManager;
    private boolean isLogado = false;
    private List<Request> listaRequests = new ArrayList<>();
    private RecyclerView recyclerView;
    private RequestAdapter mAdapter;
    Intent intent = new Intent();
    Usuario usuario = new Usuario();
    private TextView tv;

    public Button aceita, recusa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_diarista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        aceita = (Button) (findViewById(R.id.aceita));
        recusa = (Button) (findViewById(R.id.recusa));

        recyclerView = (RecyclerView) findViewById(R.id.recycler_request);
        mAdapter = new RequestAdapter(listaRequests);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        if (isLogado == false) {
            recyclerView.setAdapter(mAdapter);
        }
        mFragmentManager = getSupportFragmentManager();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new DashboardDiarista.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                aceitaSolicitacao(view);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        prepareRequests();
    }

    public void aceitaSolicitacao(View view) {
        switch (view.getId()) {
            case R.id.aceita:
                intent = new Intent(getApplicationContext(), DashboardDiarista.class);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Solicitação Aceita", Toast.LENGTH_SHORT).show();
                break;

            case R.id.recusa:
                view.setEnabled(false);/*
                intent = new Intent(getApplicationContext(), DashboardContratante.class);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                startActivity(intent);
                this.finish();*/
                Toast.makeText(getApplicationContext(), "Solicitação Recusada", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard_diarista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        Intent intent;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == R.id.home) {
            intent = new Intent(getApplicationContext(), DashboardDiarista.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Iniciando painel Diaristas", Toast.LENGTH_SHORT).show();
        } else if(id == R.id.simple_login){
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = new Intent();
        int id = item.getItemId();

       if (id == R.id.evolucao) {
            intent = new Intent(getApplicationContext(), NivelEvolucaoDiarista.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Buscando evolução...", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.agenda) {
            intent = new Intent(getApplicationContext(), AgendaActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Opção Minha Agenda", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.historico) {
            intent = new Intent(getApplicationContext(), HistoricoActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Buscando histórico de serviços.", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.registraHorario) {
            intent = new Intent(getApplicationContext(), RegistraHorarioActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Opção de registro.", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_share) {
           Toast.makeText(getApplicationContext(), "Breve você poderá divulgar sua evolução no Facebook.", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //método que busca os requests realizados para esse usuário
    private void prepareRequests() {
        String url = "https://diaristas.herokuapp.com/api/request";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                try {
                    if (response.length() > 0) {
                        listaRequests.clear();
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            Request request = new Request();
                            request.horario = jsonObject.getString("horario");
                            request.endereco = jsonObject.getString("endereco");
                            request.data = jsonObject.getString("dataPedido");
                            request.usuarioRequerente = jsonObject.getString("diarista");

                            listaRequests.add(i, request);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
        };
        Volley.newRequestQueue(this).add(jsonArrayRequest);
    }

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private DashboardDiarista.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final DashboardDiarista.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) { return true; }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private void toggleFragment(int index) {
        Fragment fragment = mFragmentManager.findFragmentByTag(FRAGMENT_TAG);
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        switch (index){
            case LOGIN:
                transaction.replace(android.R.id.content, new FragmentFacebookLogin(), FRAGMENT_TAG);
                break;
        }
        transaction.commit();
    }
}