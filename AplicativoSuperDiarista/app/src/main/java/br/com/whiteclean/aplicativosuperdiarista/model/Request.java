package br.com.whiteclean.aplicativosuperdiarista.model;

import java.util.Date;

/**
 * Created by I844129 on 10/05/2016.
 */
public class Request {

    public Boolean aceito;
    public String data;
    public String endereco;
    public String horario;
    public String usuarioRequerente;
    public String usuarioRequerido;

    public Request() {
    }

    public Request(Boolean aceito, String data, String usuarioRequerente, String usuarioRequerido) {
        this.aceito = aceito;
        this.data = data;
        this.usuarioRequerente = usuarioRequerente;
        this.usuarioRequerido = usuarioRequerido;
    }

    public Request(String horario, String endereco){
        this.endereco = endereco;
        this.horario = horario;
    }
    public Boolean getAceito() {
        return aceito;
    }

    public void setAceito(Boolean aceito) {
        this.aceito = aceito;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUsuarioRequerente() {
        return usuarioRequerente;
    }

    public void setUsuarioRequerente(String usuarioRequerente) {
        this.usuarioRequerente = usuarioRequerente;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getUsuarioRequerido() {
        return usuarioRequerido;
    }

    public void setUsuarioRequerido(String usuarioRequerido) {
        this.usuarioRequerido = usuarioRequerido;
    }

    @Override
    public String toString() {
        return "Request{" +
                "aceito=" + aceito +
                ", data='" + data + '\'' +
                ", endereco='" + endereco + '\'' +
                ", horario='" + horario + '\'' +
                ", usuarioRequerente=" + usuarioRequerente +
                ", usuarioRequerido=" + usuarioRequerido +
                '}';
    }
}
