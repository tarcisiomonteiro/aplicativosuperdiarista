package br.com.whiteclean.aplicativosuperdiarista;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class SMSActivity extends AppCompatActivity implements OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_layout);

        findViewById(R.id.envia).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        iniciaPedido(v);

        Toast.makeText(getApplicationContext(), "Seu pedido foi enviado com sucesso. " +
                "O diarista solicitado responderá assim que receber a notificação", Toast.LENGTH_SHORT).show();
        String phoneNumber = ((EditText) findViewById(R.id.editView1)).getText().toString();

        try {
            SmsManager.getDefault().sendTextMessage("+555193954328", null,
                    "Olá, gostaria de saber sua disponibilidade.", null, null);
        } catch (Exception e) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            AlertDialog dialog = alertDialogBuilder.create();

            dialog.setMessage(e.getMessage());

            dialog.show();
        }
    }

   /* private class GetDataTask extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SMSActivity.this);
            progressDialog.setMessage("Carregando dados... Por favor aguarde.");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder rs = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(1000);
                connection.setConnectTimeout(1000);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    rs.append(line).append("\n");
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                return "Problema na conexão";
            }
            return rs.toString();
        }

        @Override
        protected void onPostExecute(String result){

            Toast.makeText(getApplicationContext(), "Nenhuma nova solicitação", Toast.LENGTH_SHORT).show();
//        tv.setText(result);

            if (progressDialog != null){
                progressDialog.dismiss();
            }
            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }
    }*/

    public void iniciaPedido(View view){
        Intent intent = new Intent(getApplicationContext(), DashboardContratante.class);
        startActivity(intent);

        String url = "https://diaristas.herokuapp.com/api/message";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response).getJSONObject("form");
                    String site = jsonResponse.getString("site"), network = jsonResponse.getString("network");
                    System.out.println("Site: " + site + "\nNetwork: " + network);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("text", "Olá, gostaria de saber se você tem disponibilidade para realizar uma limpeza em minha casa.");
                params.put("nome", "Tarcisio Monteiro");

                return params;
            }
        };
        Volley.newRequestQueue(this).add(postRequest);
    }
}
