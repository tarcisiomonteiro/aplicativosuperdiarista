package br.com.whiteclean.aplicativosuperdiarista.Services;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.util.Log;

public class GetUsuario extends AsyncTask<String , Integer, JSONObject>{

    @Override
    protected JSONObject doInBackground(String... URL) {
        URL url = null;
        String jsonString = "";
        JSONObject jsonObject = null;

        // Cria o objeto url
        try {
            url = new URL(URL[0]);
            Log.d("Connection", "Successful");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // Conecta com o url pra realizar input e output de dados.
        URLConnection urlConnection = null;
        try {
            urlConnection = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Pega o input stream de dados da url, e passa para uma string.
        if (urlConnection.getDoInput()) {
            InputStream inputStream = null;
            try {
                inputStream = new BufferedInputStream(urlConnection.getInputStream());
                jsonString = readStream(inputStream);
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // Transforma a string da url para um array de JSON.
        try {
            if (!jsonString.equals("")) {
                jsonObject = new JSONObject(jsonString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * Recebe um input stream, carrega todos dados inseridos nele em um string
     * builder e retorna um json string.
     *
     * @param inputStream
     * @return
     */
    private static String readStream(InputStream inputStream) {
        String line;
        BufferedReader bfr = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();

        try {
            while ((line = bfr.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return total.toString();
    }

}
