package br.com.whiteclean.aplicativosuperdiarista;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.model.Usuario;

public class EscolhaActivity extends AppCompatActivity {

    private List<Usuario> listaUsuarios = new ArrayList<>();
    public static final int LOGIN = 0;
    private static final String STATE_SELECTED_FRAGMENT_INDEX = "selected_fragment_index";
    public static final String FRAGMENT_TAG = "fragment_tag";
    private FragmentManager mFragmentManager;
    private boolean isLogado = false;
    Intent intent = new Intent();
    Usuario usuario = new Usuario();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFragmentManager = getSupportFragmentManager();

        getUsuario();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_escolha, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        Intent intent;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == R.id.simple_login) {
         /*   Toast.makeText(getApplicationContext(), "Escolha um perfil, depois entre com seus dados do Facebook...", Toast.LENGTH_SHORT).show();*/
            isLogado = true;
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Método que busca os dados de usuários para decisão da tela de login ou escolha de perfil.
    public void getUsuario() {

        String url = "https://diaristas.herokuapp.com/api/usuario";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>(){
            //StringRequest postRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>()
            @Override
            public void onResponse(JSONArray response) {
                try {
                    if (response.length() > 0) {
                        listaUsuarios.clear();
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            Usuario usuario = new Usuario();
                            usuario.setNome(jsonObject.getString("username"));
                            usuario.setTelefone(jsonObject.getString("telefone"));

                            listaUsuarios.add(i, usuario);

                            System.out.println("Usuarios: " + usuario.getUsername() +usuario.getTelefone());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
        };
        Volley.newRequestQueue(this).add(jsonArrayRequest);
    }

    public void escolhePerfil(View view){

        switch (view.getId()){
            case R.id.card_diarista:
                intent = new Intent(getApplicationContext(), DashboardDiarista.class);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                usuario.setTipoUsuario("diarista");
                intent.putExtra("TipoUsuario", usuario.getTipoUsuario());
                Toast.makeText(getApplicationContext(), "Carregando o perfil de Diarista" + usuario.getNome(), Toast.LENGTH_SHORT).show();
                startActivity(intent);
                this.finish();
                break;

            case R.id.card_contratante:
                intent = new Intent(getApplicationContext(), DashboardContratante.class);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                usuario.setTipoUsuario("diarista");
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Carregando o perfil de Contratante", Toast.LENGTH_SHORT).show();
                this.finish();
                break;
        }
    }

  /*  public void escolheContratante(View v) { 
        intent = new Intent(getBaseContext(), DashboardContratante.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        Toast.makeText(getApplicationContext(), "Carregando o perfil de Contratante", Toast.LENGTH_SHORT).show();
        finish();
    *//*
        if(isLogado == true) {
        }else {
            intent = new Intent(getBaseContext(), LoginActivity.class);
            intent.putExtra("perfil", "Contratante");
            startActivity(intent);
        }*//*
    }

    public void escolheDiarista(View v) {
        intent = new Intent(getApplicationContext(), DashboardDiarista.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
     *//*   if(isLogado == true) {
        } else{
            Intent intent = new Intent(getBaseContext(), LoginActivity.class);
            intent.putExtra("perfil", "Diarista");
            startActivity(intent);
        }*//*

        this.finish();
    }*/
}
