package br.com.whiteclean.aplicativosuperdiarista.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Rafael on 24/01/2016.
 */
public class Diarista {

    public long id;
    public String nome;
    public String email;
    public String telefone;
    public String cidade;
    public String nivel;
    public List<Date> disponiblidade;
    public static int pontuacao;
    public double avaliacao;
    public String urlFoto;

    public Diarista(String nome, String email, String telefone, String cidade, String nivel, double avaliacao, List<Date> disponiblidade) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.cidade = cidade;
        this.avaliacao = avaliacao;
        this.nivel = nivel;
        this.disponiblidade = disponiblidade;
    }

    public Diarista(long id, String nome, String email, String telefone, String cidade, String nivel, int pontuacao, String urlFoto) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.cidade = cidade;
        this.nivel = nivel;
        this.pontuacao = pontuacao;
        this.urlFoto = urlFoto;
    }

    public Diarista() {
    }

    public Diarista(String nome, String email, String telefone) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }
    //Manda valores para a classes de Registro e Avaliação
    public String setNivel(){

        if (pontuacao <= 200){
            nivel = String.valueOf(NIVEL_DESCRI.MINIMO);
        } else if (pontuacao > 200 && pontuacao <= 400){
            nivel =  String.valueOf(NIVEL_DESCRI.BASICO);
        } else if (pontuacao > 400 && pontuacao <= 600){
            nivel =  String.valueOf(NIVEL_DESCRI.AJUDANTE);
        } else if (pontuacao > 600 && pontuacao <= 800){
            nivel =  String.valueOf(NIVEL_DESCRI.JUNIOR);
        } else if (pontuacao > 800 && pontuacao <= 1000){
            nivel =  String.valueOf(NIVEL_DESCRI.EXPERT);
        } else if (pontuacao > 1000 && pontuacao <= 1200){
            nivel =  String.valueOf(NIVEL_DESCRI.SENIOR);
        } else if (pontuacao > 1200 && pontuacao <= 1400){
            nivel =  String.valueOf(NIVEL_DESCRI.MASTER);
        } else if (pontuacao > 1401 && pontuacao <= 1600){
            nivel =  String.valueOf(NIVEL_DESCRI.ULTRA);
        }else if (pontuacao > 1601) {
            nivel =  String.valueOf(NIVEL_DESCRI.SUPERDIARISTA);
        }
     return nivel;
    }

    public double getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(double avaliavao) {
        this.avaliacao = avaliavao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public List<Date> getDisponiblidade() {
        return disponiblidade;
    }

    public void setDisponiblidade(List<Date> disponiblidade) {
        this.disponiblidade = disponiblidade;
    }

    public static int getPontuacao() {
        return pontuacao;
    }

    public static void setPontuacao(int pontuacao) {
        Diarista.pontuacao = pontuacao;
    }

    public String getNivel() {
        return nivel;
    }

    @Override
    public String toString() {
        return "Diarista{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", telefone='" + telefone + '\'' +
                ", cidade='" + cidade + '\'' +
                ", nivel=" + nivel +
                ", pontuacao=" + pontuacao +
                ", urlFoto='" + urlFoto + '\'' +
                '}';
    }
}