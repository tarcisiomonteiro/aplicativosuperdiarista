package br.com.whiteclean.aplicativosuperdiarista;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;

public class NivelEvolucaoDiarista extends AppCompatActivity {

    private List<Diarista> listaEvDiarista = new ArrayList<>();
    Diarista diarista = new Diarista();
    private RecyclerView recyclerView;
    private DiaristaAdapter mAdapter;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel_evolucao);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_evolucao);
        mAdapter = new DiaristaAdapter(listaEvDiarista);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new ListaDiaristas.RecyclerTouchListener(getApplicationContext(), recyclerView, new ListaDiaristas.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(getApplicationContext(), SolicitaServicoActivity.class);
                startActivity(intent);
                listaEvDiarista.get(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        prepareDiaristas();
    }

    private void prepareDiaristas() {
        String url = "https://diaristas.herokuapp.com/api/usuario";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                try {
                    if (response.length() > 0) {
                        listaEvDiarista.clear();
                        for (int i = 0; i < 1; i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            Diarista diarista = new Diarista();
                            diarista.nome = jsonObject.getString("nome");
                            diarista.email = jsonObject.getString("email");
                            diarista.telefone = jsonObject.getString("telefone");

                            Toast.makeText(getApplicationContext(), "Bem vinda " + diarista.nome, Toast.LENGTH_SHORT).show();

                            listaEvDiarista.add(i, diarista);


                        }
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
        };
        Volley.newRequestQueue(this).add(jsonArrayRequest);
    }
}
/*
    private class GetDataTask extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NivelEvolucaoDiarista.this);
            progressDialog.setMessage("Carregando dados... Por favor aguarde.");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder rs = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(1000);
                connection.setConnectTimeout(1000);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    rs.append(line).append("\n");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                return "Problema na conexão";
            }
            return rs.toString();
        }

        @Override
        protected void onPostExecute(String result){

            Toast.makeText(getApplicationContext(), "Resultado vazio", Toast.LENGTH_SHORT).show();
            tv.setText(result);
            if (progressDialog != null){
                progressDialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }*/
