package  br.com.whiteclean.aplicativosuperdiarista.model;

import java.util.Date;

public class AgendaServico {
	
	private int idAgendaServico;
	private Servico idServico;
	private Date dataAgendamento;
	private String horario;
	
	
	public AgendaServico(int idAgendaServico, Servico idServico, Date dataAgendamento, String horario) {
		this.idAgendaServico = idAgendaServico;
		this.idServico = idServico;
		this.dataAgendamento = dataAgendamento;
		this.horario = horario;
	}


	public int getIdAgendaServico() {
		return idAgendaServico;
	}


	public void setIdAgendaServico(int idAgendaServico) {
		this.idAgendaServico = idAgendaServico;
	}


	public Servico getIdServico() {
		return idServico;
	}


	public void setIdServico(Servico idServico) {
		this.idServico = idServico;
	}


	public Date getDataAgendamento() {
		return dataAgendamento;
	}


	public void setDataAgendamento(Date dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}


	public String getHorario() {
		return horario;
	}


	public void setHorario(String horario) {
		this.horario = horario;
	}


	@Override
	public String toString() {
		return "AgendaServico [idAgendaServico=" + idAgendaServico
				+ ", idServico=" + idServico + ",  horario=" + horario + "]";
	}
	
}
