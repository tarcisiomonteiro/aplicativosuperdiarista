package br.com.whiteclean.aplicativosuperdiarista.model;

public class Avatar {
	
	private int idAvatar;
	private int nivel;
	private String imagem;
	
	public Avatar(int idAvatar, int nivel, String imagem) {
		this.idAvatar = idAvatar;
		this.nivel = nivel;
		this.imagem = imagem;
	}

	public int getIdAvatar() {
		return idAvatar;
	}


	public void setIdAvatar(int idAvatar) {
		this.idAvatar = idAvatar;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}


	@Override
	public String toString() {
		return "Avatar [idAvatar=" + idAvatar + ", nivel=" + nivel
				+ ", imagem=" + imagem + "]";
	}
	
}
