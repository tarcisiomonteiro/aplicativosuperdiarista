package br.com.whiteclean.aplicativosuperdiarista.Services;

import android.content.Context;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;

/**
 * Created by Rafael on 14/03/2016.
 */

public class DiaristaService {

    private static final boolean LOG_ON = false;
    private static final String TAG = "DiaristaService";

    //URL que traz a lista de diaristas do Web Service (MongoDB).
    private static final String URL = "http://diaristas.herokuapp.com/api/usuario";

    public static List<Diarista> getDiaristas(Context context){
          /*     List<Diarista> diaristas = new ArrayList<Diarista>();
        for (int i = 0; i < 2; i++){
            Diarista d = new Diarista();
            d.nome = "Nome" + i;
            d.email = "Email" + i;
            d.urlFoto = "http://www.livroandroid.com.br/livro/carros/esportivos/Ferrari_FF.png";
            diaristas.add(d);
        }*/

        try{
            String json = readFile(context);
            List<Diarista> diaristas = parserJSON(context, json);
            return diaristas;
        } catch (Exception e){
            Log.e(TAG, "Erro:" + e.getMessage(), e);
            return null;
        }
    }
    //leitura arquivo raw
    private static String readFile(Context context) throws IOException{
            return null; // FileUtils.readRawFileString(context, R.raw.diaristas, "UTF-8");
        }

    //parse json
    private static List<Diarista> parserJSON(Context context, String json) throws IOException {
        List<Diarista> diaristas = new ArrayList<>();
        try{
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("diaristas");
            JSONArray jsonDiaristas = obj.getJSONArray("diarista");
            //insere as diaristas na lista
            for (int i = 0; i < jsonDiaristas.length(); i++){
                JSONObject jsonDiari = jsonDiaristas.getJSONObject(i);
                Diarista d = new Diarista();
                d.nome = jsonDiari.optString("nome");
                d.email = jsonDiari.optString("email");
                d.telefone = jsonDiari.optString("telefone");
                d.cidade = jsonDiari.optString("cidade");
                if(LOG_ON){
                    Log.d(TAG, "Diarista" + d.nome);
                }
                diaristas.add(d);
            }
            if (LOG_ON){
                Log.d(TAG, diaristas.size()+"encontrados");
            }
        }catch (JSONException e){
            throw new IOException(e.getMessage(), e);
        }
        return diaristas;
    }
}
