package br.com.whiteclean.aplicativosuperdiarista;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.model.Usuario;

/**
 * Created by Rafael on 07/11/2016.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {

    private List<Date> listaDatas;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView dia, mes, ano;

        public MyViewHolder(View view) {
            super(view);

            dia = (TextView) view.findViewById(R.id.data);
            mes = (TextView) view.findViewById(R.id.endereco);
            ano = (TextView) view.findViewById(R.id.horario);
        }
    }
        public DataAdapter(List<Date> listaDatas) {
            this.listaDatas = listaDatas;
        }

    @Override
    public DataAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(DataAdapter.MyViewHolder holder, int position) {
        Date date = listaDatas.get(position);
        holder.dia.setText(date.getDay());
        holder.mes.setText(date.getMonth());
        holder.ano.setText(date.getYear());
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
