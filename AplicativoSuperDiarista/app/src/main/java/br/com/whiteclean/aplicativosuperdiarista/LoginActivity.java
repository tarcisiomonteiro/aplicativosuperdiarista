package br.com.whiteclean.aplicativosuperdiarista;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import br.com.whiteclean.aplicativosuperdiarista.fragments.FragmentFacebookLogin;
import br.com.whiteclean.aplicativosuperdiarista.model.Device;
import br.com.whiteclean.aplicativosuperdiarista.model.Usuario;


public class LoginActivity extends AppCompatActivity {

    public static final int LOGIN = 0;
    public static final String FRAGMENT_TAG = "fragment_login";
    private FragmentManager mFragmentManager;
    private boolean isLogado = false;

    public String deviceId;
    Intent intent = new Intent();
    Usuario usuario = new Usuario();
    String perfil;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        intent = getIntent();

        String nome = intent.getStringExtra("nome");
        usuario.setNome(nome);

        deviceId = intent.getStringExtra("deviceId");
        Toast.makeText(getApplicationContext(), deviceId, Toast.LENGTH_SHORT).show();

        tv = (TextView) findViewById(R.id.tv);
        tv.setText(perfil);

        mFragmentManager = getSupportFragmentManager();
        toggleFragment(LOGIN);
/*
        intent = new Intent(getBaseContext(), EscolhaActivity.class);
        startActivity(intent);
*/
    }

    public void iniciaContratacao(View v) {
        Intent intent = new Intent(getApplicationContext(), DashboardContratante.class);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), "Legal, você agora está conectado.", Toast.LENGTH_SHORT).show();
    }

    private void toggleFragment(int index) {
        Fragment fragment = mFragmentManager.findFragmentByTag(FRAGMENT_TAG);
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        switch (index){
            case LOGIN:
                transaction.replace(android.R.id.content, new FragmentFacebookLogin(), FRAGMENT_TAG);
                break;
        }
        transaction.commit();
    }
}
