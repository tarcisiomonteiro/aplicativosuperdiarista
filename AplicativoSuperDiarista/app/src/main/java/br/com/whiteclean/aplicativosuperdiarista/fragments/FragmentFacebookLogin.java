package br.com.whiteclean.aplicativosuperdiarista.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import br.com.whiteclean.aplicativosuperdiarista.DashboardDiarista;
import br.com.whiteclean.aplicativosuperdiarista.EscolhaActivity;
import br.com.whiteclean.aplicativosuperdiarista.LoginActivity;
import br.com.whiteclean.aplicativosuperdiarista.R;
import br.com.whiteclean.aplicativosuperdiarista.model.Usuario;

public class FragmentFacebookLogin extends Fragment {

        private TextView mTextDetails;
        private CallbackManager mCallbackManager;
        private AccessTokenTracker mTokenTracker;
        private ProfileTracker mProfileTracker;
        private ProfilePictureView picview;
        Intent intent = new Intent();
        Usuario usuario = new Usuario();
        private boolean isLogado = false;

        private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("Diaristas", "onSuccess");
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                mTextDetails.setText(constructWelcomeMessage(profile));
                accessToken.getToken();
                isLogado = true;
                intent.putExtra("Usuario", usuario.toString());
            }

            @Override
            public void onCancel() {
                Log.d("Diaristas", "onCancel");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("Diaristas", "onError " + e);
            }
        };

        public FragmentFacebookLogin() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            FacebookSdk.sdkInitialize(this.getContext());

            mCallbackManager = CallbackManager.Factory.create();
            setupTokenTracker();
            setupProfileTracker();

            mTokenTracker.startTracking();
            mProfileTracker.startTracking();

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_facebook_login, container, false);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            setupTextDetails(view);
            setupLoginButton(view);
            picview = (ProfilePictureView) view.findViewById(R.id.pic_view);
        }

        @Override
        public void onResume() {
            super.onResume();
            Profile profile = Profile.getCurrentProfile();
            mTextDetails.setText(constructWelcomeMessage(profile));
        }

        @Override
        public void onStop() {
            super.onStop();
            mTokenTracker.stopTracking();
            mProfileTracker.stopTracking();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            mCallbackManager.onActivityResult(requestCode, resultCode, data);

        }

        private void setupTextDetails(View view) {
            mTextDetails = (TextView) view.findViewById(R.id.text_details);
        }

        private void setupTokenTracker() {
            mTokenTracker = new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                    Log.d("Diaristas", "" + currentAccessToken);
                }
            };
        }

        private void setupProfileTracker() {
            mProfileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                    Log.d("Diaristas", "" + currentProfile);
                    mTextDetails.setText(constructWelcomeMessage(currentProfile));
                }
            };
        }

        private void setupLoginButton(View view) {
            LoginButton mButtonLogin = (LoginButton) view.findViewById(R.id.login_button);
            mButtonLogin.setFragment(this);
            mButtonLogin.setReadPermissions("user_friends");
            mButtonLogin.registerCallback(mCallbackManager, mFacebookCallback);
        }

        @NonNull
        private String constructWelcomeMessage(Profile profile) {
            StringBuffer stringBuffer = new StringBuffer();
            if (profile != null) {

                stringBuffer.append("Bemvindo " + profile.getName() + "\n");
                stringBuffer.append("Seu Nome será:" + profile.getFirstName());

                Bundle arguments = new Bundle();
                arguments.putString("nome", usuario.getNome());

                usuario.setNome(profile.getName());
                usuario.setId(profile.getId());
                intent.putExtra("nome", arguments);
                Toast.makeText(getContext(), "Usuário identificado: " + usuario.getNome(), Toast.LENGTH_SHORT).show();
            } else{
                Toast.makeText(getContext(), "Usuário desconhecido", Toast.LENGTH_SHORT).show();
            }
            return stringBuffer.toString();
    }
}