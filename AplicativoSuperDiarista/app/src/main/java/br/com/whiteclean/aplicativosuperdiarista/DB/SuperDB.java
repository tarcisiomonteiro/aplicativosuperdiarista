package br.com.whiteclean.aplicativosuperdiarista.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import  br.com.whiteclean.aplicativosuperdiarista.model.Diarista;

import java.lang.Object;import java.lang.Override;import java.lang.String;import java.util.ArrayList;
import java.util.List;

/**
 * Created by I844129 on 01/12/2015.
 */
public class SuperDB extends SQLiteOpenHelper {

    private static final String TAG = "sql";
    private static final String NOME_BANCO = "superdiaristas";
    private static final int VERSAO_BANCO = 1;


    public SuperDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }
    public SuperDB(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.d(TAG, "Criando a tabela no banco de dados...");
        db.execSQL("create table if not exists diarista (_id integer primary key autoincrement, nome text, email text, telefone text, endereco text);");

        Log.d(TAG, "Tabela diarista criada com sucesso");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {   }

    public long save(Diarista diarista) {

        long id = diarista.id;

        SQLiteDatabase db = getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put("nome", diarista.nome);
            values.put("email", diarista.email);
            values.put("telefone", diarista.telefone);
            values.put("endereco", diarista.cidade);

            if (id != 0) {
                String _id = String.valueOf(diarista.id);
                String[] whereArgs = new String[]{_id};

                int count = db.update("diarista", values, "_id=?", whereArgs);
                return count;
            } else {
                return id;
            }
        } finally {
            db.close();
        }
    }

    public int delete (Diarista diarista) {
        SQLiteDatabase db = getWritableDatabase();

        try{
            int count = db.delete("diarista", "_id=?", new String[]{String.valueOf(diarista.id)});
            Log.i(TAG, "Deletou [" + count + "] registro.");
            return count;
        }finally {
            db.close();
        }
    }

    public List<Diarista> findAll(){
        SQLiteDatabase db = getWritableDatabase();

        try{
            Cursor c = db.query("diarista", null, null, null, null, null, null, null);
            return toList(c);
        } finally {
            db.close();
        }
    }

    private List<Diarista> toList(Cursor c) {
        List<Diarista> diaristas = new ArrayList<Diarista>();
        if (c.moveToFirst()){
            do {
                Diarista diarista = new Diarista();
                diaristas.add(diarista);
                diarista.id = c.getLong(c.getColumnIndex("_id"));
                diarista.nome = c.getString(c.getColumnIndex("nome"));
                diarista.email = c.getString(c.getColumnIndex("email"));
                diarista.telefone = c.getString(c.getColumnIndex("telefone"));
                diarista.cidade = c.getString(c.getColumnIndex("cidade"));

            }while (c.moveToNext());
        }
        return diaristas;
    }

    //método que executa um comando SQL
    public void execSQL (String sql){
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL(sql);

        } finally {
            db.close();
        }
    }

    public void execSQL (String sql, Object[] args){
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL(sql, args);

        } finally {
            db.close();
        }
    }
}