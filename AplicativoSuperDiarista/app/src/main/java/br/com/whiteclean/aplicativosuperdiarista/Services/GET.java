package br.com.whiteclean.aplicativosuperdiarista.Services;

import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import br.com.whiteclean.aplicativosuperdiarista.model.Bairro;
import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;
import br.com.whiteclean.aplicativosuperdiarista.model.NIVEL_DESCRI;

public class GET {
    private static final String URL = "https://diaristas.herokuapp.com/api/usuario";

    public GET() {  }

    public static List<Diarista> getDiaristas(String string) {
        List<Diarista> diarista = new ArrayList<>();

        JSONObject jsonObjecDiarista = null;
        try {
            jsonObjecDiarista = new GetUsuario().execute(URL).get();
            Log.d("Working connection", URL);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }

        JSONArray jsonArrayDiarista = null;
        try {
            assert jsonObjecDiarista != null;
            jsonArrayDiarista = jsonObjecDiarista.getJSONArray("diaristas");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < jsonArrayDiarista.length(); i++) {
            try {

                JSONObject jsonObjectDiarista = jsonArrayDiarista.getJSONObject(i);

                String nome = jsonObjectDiarista.getString("nome");
                String email = jsonObjectDiarista.getString("email");
                String fone = jsonObjectDiarista.getString("fone");
                String cidade = jsonObjectDiarista.getString("cidade");
                String nivel =  jsonObjectDiarista.getString("nivel");
                Boolean dia =  jsonObjectDiarista.getBoolean("boolean");
                List<Date> disponibilidade = (List<Date>) jsonObjectDiarista.getJSONArray("disponibilidade");
                double avaliacao = jsonObjectDiarista.getDouble("avaliacao");

                diarista.add(i, new Diarista(nome, email, fone, cidade, nivel, avaliacao, disponibilidade));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return diarista;
    }

/*    private class GetDataTask extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RegistraTipoServico.this);
            progressDialog.setMessage("Carregando dados... Por favor aguarde.");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder rs = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(1000);
                connection.setConnectTimeout(1000);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    rs.append(line).append("\n");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                return "Problema na conexão";
            }
            return rs.toString();
        }

        @Override
        protected void onPostExecute(String result){

            Toast.makeText(getApplicationContext(), "Resultado vazio", Toast.LENGTH_SHORT).show();
            tv.setText(result);
            if (progressDialog != null){
                progressDialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }*/


    /*

    public static List<Diarista> getDiaristasPorBairro(int idBairro) {
        List<Diarista> diarista = new ArrayList<Diarista>();

        JSONObject jsonObjecDiarista = null;
        try {
            jsonObjecDiarista = new GetUsuario().execute(URL + "/diarista/" + idBairro).get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }

        JSONArray jsonArrayDiarista = null;
        try {
            jsonArrayDiarista = jsonObjecDiarista.getJSONArray("diarista");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < jsonArrayDiarista.length(); i++) {
            try {

                JSONObject jsonObjectDiarista = jsonArrayDiarista
                        .getJSONObject(i);

                int idDiarista = jsonObjectDiarista.getInt("idDiarista");
                String nome = jsonObjectDiarista.getString("nome");
                int tipoUsuario = jsonObjectDiarista.getInt("tipoUsuario");
                String email = jsonObjectDiarista.getString("email");
                String fone = jsonObjectDiarista.getString("fone");
                Nivel nivel = jsonObjectDiarista.optString("nivel");
                String endereco = jsonObjectDiarista.getString("endereco");

                Bairro bairro = getBairro("idBairro");

                diarista.add(i, new Diarista(long id, String nome, String email, String fone, String endereco, Nivel
                nivel, String urlFoto));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }return diarista;
    }
*/
    private static Bairro getBairro(String string) {

        return null;
    }

    public static String getDeviceId() {
        return null;
    }
    /*

    public static List<Bairro> getBairros(String string) {
        List<Bairro> bairros = new ArrayList<Bairro>();

        JSONObject jsonObjecBairro = null;
        try {
            jsonObjecBairro = new GetUsuario().execute(URL + "/bairros/")
                    .get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }

        JSONArray jsonArrayBairro = null;
        try {
            jsonArrayBairro = jsonObjecBairro.getJSONArray("bairro");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < jsonArrayBairro.length(); i++) {
            try {

                JSONObject jsonObjectBairro = jsonArrayBairro.getJSONObject(i);

                int idBairro1 = jsonObjectBairro.getInt("idBairro");
                String nomeBairro = jsonObjectBairro.getString("nome");

                Cidade cidade = getCidade("idCidade");

                bairros.add(i, new Bairro(idBairro1, nomeBairro, cidade));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }return bairros;
    }

    private static Cidade getCidade(String string) {

        return null;
    }*/
}
