package br.com.whiteclean.aplicativosuperdiarista.model;

/**
 * Created by Rafael on 24/01/2016.
 */
public enum NIVEL_DESCRI {

    MINIMO,
    BASICO,
    AJUDANTE,
    JUNIOR,
    EXPERT,
    SENIOR,
    MASTER,
    ULTRA,
    SUPERDIARISTA;
}
