package br.com.whiteclean.aplicativosuperdiarista;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.com.whiteclean.aplicativosuperdiarista.model.Device;
import br.com.whiteclean.aplicativosuperdiarista.model.Diarista;
import br.com.whiteclean.aplicativosuperdiarista.model.NIVEL_DESCRI;
import br.com.whiteclean.aplicativosuperdiarista.model.Servico;
import br.com.whiteclean.aplicativosuperdiarista.model.Usuario;

public class SolicitaServicoActivity extends AppCompatActivity {

    public Servico servico = new Servico();
    public Usuario usuario = new Usuario();
    Date today = new Date();
    public String deviceId;
    Device device = new Device();
    public Diarista diarista = new Diarista();
    String texto, endereco;
    public br.com.whiteclean.aplicativosuperdiarista.model.Request request = new br.com.whiteclean.aplicativosuperdiarista.model.Request();
    private Button buttonEnvia;

    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicita);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        device.setId(deviceId);

        buttonEnvia = (Button) findViewById(R.id.solicita);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista_diarista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        Intent intent;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == R.id.pagina_inicial) {
            intent = new Intent(getApplicationContext(), DashboardContratante.class);
            startActivity(intent);
            this.finish();
            Toast.makeText(getApplicationContext(), "Iniciando painel Contratante", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void iniciaPedido(View view){
        Intent intent = new Intent(getApplicationContext(), DashboardContratante.class);
        startActivity(intent);

        String url = "https://diaristas.herokuapp.com/api/request";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response).getJSONObject("form");
                    String site = jsonResponse.getString("site"), network = jsonResponse.getString("network");
                    System.out.println("Site: " + site + "\nNetwork: " + network);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("aceito", String.valueOf(false));
                params.put("diarista", "Tarcisio Monteiro");
                params.put("endereco", "Rua Coronel Genuino, 130");
                params.put("horario", "Tarde");

                return params;
            }
        };
        Volley.newRequestQueue(this).add(postRequest);
        this.finish();
    }
}
