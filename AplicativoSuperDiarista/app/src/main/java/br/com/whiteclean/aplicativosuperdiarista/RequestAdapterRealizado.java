
package br.com.whiteclean.aplicativosuperdiarista;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.whiteclean.aplicativosuperdiarista.model.Request;

public class RequestAdapterRealizado extends RecyclerView.Adapter<RequestAdapterRealizado.MyViewHolder> {
    private List<Request> listaRequest;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView data, endereco, horario, usuario;
        /*public Usuario usuario;*/

        public MyViewHolder(View view) {
            super(view);
            data = (TextView) view.findViewById(R.id.data);
            endereco = (TextView) view.findViewById(R.id.endereco);
            horario = (TextView) view.findViewById(R.id.horario);
            usuario = (TextView) view.findViewById(R.id.usuario);
        }
    }

    public RequestAdapterRealizado(List<Request> listaRequest) {
        this.listaRequest = listaRequest;
    }

    @Override
    public RequestAdapterRealizado.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.request_aceito_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Request request = listaRequest.get(position);
        holder.data.setText(String.format("Data: %s", request.getData()));
        holder.endereco.setText(String.format("Endereço: %s", request.getEndereco()));
        holder.horario.setText(String.format("Horario: %s", request.getHorario()));
        holder.usuario.setText(String.format("Usuario: %s", request.getUsuarioRequerente()));
    }

    @Override
    public int getItemCount() {
        return listaRequest.size();
    }
}

