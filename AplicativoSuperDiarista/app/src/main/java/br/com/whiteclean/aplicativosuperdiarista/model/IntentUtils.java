package br.com.whiteclean.aplicativosuperdiarista.model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Rafael on 22/04/2016.
 */
public class IntentUtils {

    public static void sendSms(Context context, String fone, String msg){
        Uri uri = Uri.parse("sms:" + fone);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", msg);
        context.startActivity(intent);
    }
}
